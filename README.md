# Trello challenge for an iOS developer

The following project is about a Trello challenge available online. [Link to Trello challenge](https://trello.com/jobs/ios-developer).

This challenge is about finding a 8 characters key based on a hash.

One option is a brute force attack, i.e. trying all possible solutions. This approach however will take way over 15 days on a Mac Pro machine with 8 cores.

Another option is to incrementally increase the accuracy of the search. This approach leeds to results within less than a second. This project is an example of how to do it. This project also illustrates how to use extensions (```extension NSTimeInterval```) and passing a function as a parameter (```calculateElapsedTimeOf```) with parameters.

**Algorithms can have a big impact on your application performances.**

## Trello challenge description

> Write code to find a 8 letter string of characters that contains only letters from ```acdegilmnoprstuw``` such that the hash(the_string) is ```25377615533200```.

The pseudo-code for the hash method looks like this:

```
Int64 hash (String s) {
    Int64 h = 7
    String letters = "acdegilmnoprstuw"
    for(Int32 i = 0; i < s.length; i++) {
        h = (h * 37 + letters.indexOf(s[i]))
    }
    return h
}
```

## Environment

- Xcode 7.2.1
- iOS 9
- Swift 2.x