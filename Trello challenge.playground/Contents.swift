//: The Trello challenge https://trello.com/jobs/ios-developer is about finding a key (password) for a given hash.
//:

import UIKit

let characters = Array("acdegilmnoprstuw".characters)
let leepadg = "leepadg"

//: A First implementation of the hash function as specified in the pseudo code.
func hash(value: String, usingCharacters: [Character]) -> Int64 {
    var h:Int64 = 7
    for char in value.characters {
        let value = usingCharacters.indexOf(char)!
        h = h * 37 + value
    }
    return h
}

hash(leepadg, usingCharacters: characters) == 680131659347

//: Helper functions for the optimised hash function using an array of integers.
func convertToIntArray(string: String) -> [Int] {
    var array = [Int]()
    for char in string.characters {
        let intValue = characters.indexOf(char)!
        array.append(intValue)
    }
    return array
}

convertToIntArray(leepadg) == [6, 3, 3, 10, 0, 2, 4]

func convert(intArray: [Int], usingCharacters: [Character]) -> String {
    var result = ""
    for item in intArray {
        let char = usingCharacters[item]
        result =  "\(result)\(char)"
    }
    return result
}

convertToIntArray(leepadg)
convert(convertToIntArray(leepadg), usingCharacters: characters) == leepadg

//: Optimised hash function avoiding indexOf.
func hash(array: [Int]) -> Int64 {
    var h:Int64 = 7
    for value in array {
        h = h * 37 + value
    }
    return h
}

hash(convertToIntArray(leepadg)) == 680131659347

//: Function to find a key given a hash using the optimised hash function using an array of Int.
//: The algorithm could be further optimised by using a dichotomic approach.
func findKey(withLength: Int, forHash: Int64) -> String {

    var foundCharacters = [Int](count: withLength, repeatedValue: 0)
    var index = 0
    var previousValue = foundCharacters[index]
    
    var value = hash(foundCharacters)
    while index < foundCharacters.count || value != forHash {
        if value > forHash {
            foundCharacters[index] = previousValue
            index++
        } else {
            previousValue = foundCharacters[index]
            foundCharacters[index]++
        }
        value = hash(foundCharacters)
    }
    return convert(foundCharacters, usingCharacters: characters)
}

//: Extension for formatting NSTimeInterval values in hours, minutes, seconds and millisecs.
extension NSTimeInterval {
    var time: String {
        return String(format:"%d:%02d:%02d.%03d", Int(self/3600.0), Int(self/60.0 % 60), Int(self % 60), Int(self*1000 % 1000))
    }
}

//: Example of a function taking a function as parameter and displaying the elapsed execution time of the function passed as parameter.
func calculateElapsedTimeOf(function: (Int, Int64) -> String, withLength: Int, forHash: Int64) -> (String, String) {
    let startTime = NSDate.timeIntervalSinceReferenceDate()
    let result = function(withLength, forHash)
    let endTime = NSDate.timeIntervalSinceReferenceDate()
    return (result, (endTime - startTime).time)
}

//: Test using leepadg.
let result1 = calculateElapsedTimeOf(findKey, withLength: 7, forHash: 680131659347)
hash(convertToIntArray(result1.0)) == 680131659347

let result2 = calculateElapsedTimeOf(findKey, withLength: 8, forHash: 25377615533200)
hash(convertToIntArray(result2.0)) == 25377615533200

let value3 = "wwwwwwwwwww"
let hash3 = hash(value3, usingCharacters: characters)
let result3 = calculateElapsedTimeOf(findKey, withLength: 11, forHash: hash3)
result3.0 == value3

